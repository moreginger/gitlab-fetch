export interface UploadedFile {
  alt: string;
  url: string;
  markdown: string;
}
