export type JobState = 'created' | 'pending' | 'running' | 'failed' | 'success' | 'canceled' | 'skipped';
