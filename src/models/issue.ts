import { UserSummary } from './user';
import { Milestone } from './milestone';

export interface Issue {
  state: 'opened' | 'closed';
  description: string;
  author: UserSummary;
  milestone: Milestone;
  project_id: number;
  assignee?: UserSummary;
  updated_at: string;
  id: number;
  title: string;
  created_at: string;
  iid: number;
  labels: string[];
  user_notes_count: string;
}
