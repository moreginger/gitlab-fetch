import { Commit, Diff } from './commit';

export interface CompareRepository {
  commit: Commit;
  commits: Commit[];
  diffs: Diff[];
  compare_timeout: boolean;
  compare_same_ref: boolean;
}
