import { AbstractApi, Config } from '../common';
import { Label } from '../models';
import { ProjectApi } from './project';

export interface CreateLabelParams {
  description?: string;
  priority?: number | 'null';
}

export interface EditLabelParams extends CreateLabelParams {
  new_name?: string;
  color?: string;
}

export class LabelsApi extends AbstractApi {
  constructor(config: Config, id: number);
  constructor(config: Config, namespace: string, projectName: string);
  constructor(config: Config, idOrNamespace: number|string, projectName?: string) {
    super(config, `/projects/${ProjectApi.toProjectId(idOrNamespace, projectName)}/labels`);
  }

  getAll(): Promise<Label[]> {
    return this.fetch('GET', undefined);
  }
  create(name: string, color: string, params: CreateLabelParams = {}): Promise<Label> {
    return this.fetch('POST', undefined, Object.assign({ name, color }, params));
  }
  edit(name: string, params: EditLabelParams = {}): Promise<Label> {
    return this.fetch('PUT', undefined, Object.assign({ name }, params));
  }
  delete(name: string): Promise<void> {
    return this.fetch('DELETE', undefined, { name });
  }
  subscribe(name: string): Promise<Label> {
    return this.fetch('POST', `/${encodeURIComponent(name)}/subscription`, { name });
  }
  unsubscribe(name: string): Promise<Label> {
    return this.fetch('DELETE', `/${encodeURIComponent(name)}/subscription`, { name });
  }
}
