import { AbstractApi, Config } from '../common';

import { Project } from '../models';

export interface ListProjectsParams {
    archived?: boolean;
    visibility?: 'public' | 'internal' | 'private';
    orderBy?: 'id' | 'name' | 'path' | 'created_at' | 'updated_at' | 'last_activity_at';
    sort?: 'asc' | 'desc';
    search?: string;
}

export interface CreateProjectParams {
    path?: string;
    namespace_id?: number;
    description?: string;
    issues_enabled?: boolean;
    merge_requests_enabled?: boolean;
    builds_enabled?: boolean;
    wiki_enabled?: boolean;
    snippets_enabled?: boolean;
    container_registry_enabled?: boolean;
    shared_runners_enabled?: boolean;
    public?: boolean;
    visiblity_level?: number;
    import_url?: string;
    public_builds?: boolean;
    only_allow_merge_if_build_succeeds?: boolean;
    only_allow_merge_if_all_discussions_are_resolved?: boolean;
    lfs_enabled?: boolean;
    request_access_enabled?: boolean;
}

export interface SearchProjectsParams {
    order_by?: 'id' | 'name' | 'created_at' | 'last_activity_at';
    sort?: 'asc' | 'desc';
}

export class ProjectsApi extends AbstractApi {
  constructor(config: Config) {
    super(config, '/projects');
  }

  getAll(params: ListProjectsParams = {}): Promise<Project[]> {
    return this.fetch('GET', undefined, params);
  }
  getVisible(params: ListProjectsParams = {}): Promise<Project[]> {
    return this.fetch('GET', '/visible', params);
  }
  getOwned(params: ListProjectsParams = {}): Promise<Project[]> {
    return this.fetch('GET', '/owned', params);
  }
  getStarred(params: ListProjectsParams = {}): Promise<Project[]> {
    return this.fetch('GET', '/starred', params);
  }
  getAllAdmin(params: ListProjectsParams = {}): Promise<Project[]> {
    return this.fetch('GET', '/all', params);
  }
  search(query: string, params: SearchProjectsParams = {}): Promise<Project[]> {
    return this.fetch('GET', `/projects/search/${encodeURIComponent(query)}`, params);
  }

  create(name: string, params: CreateProjectParams = {}): Promise<Project> {
    return this.fetch('POST', undefined, Object.assign({ name }, params));
  }

  createAdmin(userId: number, name: string, params: CreateProjectParams = {}): Promise<Project> {
    return this.fetch('POST', `/user/${userId}`, Object.assign({ name }, params));
  }
}
