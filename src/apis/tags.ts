import { AbstractApi, Config } from '../common';
import { Tag, Release } from '../models';
import { ProjectApi } from './project';

export interface CreateTagParams {
  message?: string;
  release_description?: string;
}

export class TagsApi extends AbstractApi {
  constructor(config: Config, id: number);
  constructor(config: Config, namespace: string, projectName: string);
  constructor(config: Config, idOrNamespace: number|string, projectName?: string) {
    super(config, `/projects/${ProjectApi.toProjectId(idOrNamespace, projectName)}/repository/tags`);
  }

  getAll(): Promise<Tag[]> {
    return this.fetch('GET', undefined);
  }

  get(name: string): Promise<Tag> {
    return this.fetch('GET', `/${encodeURIComponent(name)}`);
  }

  create(name: string, ref: string, params: CreateTagParams = {}): Promise<Tag> {
    return this.fetch('POST', undefined, Object.assign({ tag_name: name, ref }, params));
  }
  delete(name: string): Promise<void> {
    return this.fetch('DELETE', `/${encodeURIComponent(name)}`);
  }

  createRelease(name: string, description: string): Promise<Release> {
    return this.fetch('POST', `/${encodeURIComponent(name)}/release`, { description });
  }

  editRelease(name: string, description: string): Promise<Release> {
    return this.fetch('PUT', `/${encodeURIComponent(name)}/release`, { description });
  }
}
