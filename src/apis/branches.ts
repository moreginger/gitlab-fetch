import { AbstractApi, Config } from '../common';
import { Branch } from '../models';
import { ProjectApi } from './project';

export interface ProtectBranchParams {
  developers_can_push?: boolean;
  developers_can_merge?: boolean;
}

export class BranchesApi extends AbstractApi {
  constructor(config: Config, id: number);
  constructor(config: Config, namespace: string, projectName: string);
  constructor(config: Config, idOrNamespace: number|string, projectName?: string) {
    super(config, `/projects/${ProjectApi.toProjectId(idOrNamespace, projectName)}/repository/branches`);
  }

  getAll(): Promise<Branch[]> {
    return this.fetch('GET', undefined);
  }
  get(name: string): Promise<Branch> {
    return this.fetch('GET', `/${encodeURIComponent(name)}`);
  }
  protect(name: string, params: ProtectBranchParams = {}): Promise<Branch> {
    return this.fetch('PUT', `/${encodeURIComponent(name)}/protect`, params);
  }
  unprotect(name: string): Promise<Branch> {
    return this.fetch('PUT', `/${encodeURIComponent(name)}/unprotect`);
  }
  create(name: string, ref: string): Promise<Branch> {
    return this.fetch('POST', undefined, { branch_name: name, ref });
  }
  delete(name: string): Promise<void> {
    return this.fetch('DELETE', `/${encodeURIComponent(name)}`);
  }
  // deleteMergedBranches(): Promise<void> {
  //   return this.fetch('DELETE', TODO);
  // }
}
