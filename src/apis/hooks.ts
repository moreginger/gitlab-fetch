import { AbstractApi, Config } from '../common';
import { Hook } from '../models';
import { ProjectApi } from './project';

export interface AddHookParams {
  push_events?: boolean;
  issues_events?: boolean;
  merge_requests_events?: boolean;
  tag_push_events?: boolean;
  note_events?: boolean;
  build_events?: boolean;
  pipeline_events?: boolean;
  wiki_events?: boolean;
  enable_ssl_verification?: boolean;
  token?: string;
}

export interface EditHookParams extends AddHookParams {
}

export class HooksApi extends AbstractApi {
  constructor(config: Config, id: number);
  constructor(config: Config, namespace: string, projectName: string);
  constructor(config: Config, idOrNamespace: number|string, projectName?: string) {
    super(config, `/projects/${ProjectApi.toProjectId(idOrNamespace, projectName)}/hooks`);
  }

  getHooks(): Promise<Hook[]> {
    return this.fetch('GET', undefined);
  }

  getHook(hookId: number): Promise<Hook> {
    return this.fetch('GET', `/${hookId}`);
  }

  addHook(url: string, params: AddHookParams = {}): Promise<Hook> {
    return this.fetch('POST', ``, Object.assign({ url }, params));
  }
  editHook(hookId: number, url: string, params: EditHookParams = {}): Promise<Hook> {
    return this.fetch('POST', `/${hookId}`, Object.assign({ url }, params));
  }
  deleteHook(hookId: number): Promise<void> {
    return this.fetch('DELETE', `/${hookId}`);
  }
}
