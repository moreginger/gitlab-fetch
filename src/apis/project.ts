import { AbstractApi, Config } from '../common';
import { Project, UserSummary, Event, UploadedFile } from '../models';

export interface EditProjectParams {
  name?: string;
  path?: string;
  default_branch?: string;
  description?: string;
  issues_enabled?: boolean;
  merge_requests_enabled?: boolean;
  builds_enabled?: boolean;
  wiki_enabled?: boolean;
  snippets_enabled?: boolean;
  container_registry_enabled?: boolean;
  shared_runners_enabled?: boolean;
  public?: boolean;
  visiblity_level?: number;
  import_url?: string;
  public_builds?: boolean;
  only_allow_merge_if_build_succeeds?: boolean;
  only_allow_merge_if_all_discussions_are_resolved?: boolean;
  lfs_enabled?: boolean;
  request_access_enabled?: boolean;
}

export interface ShareProjectParams {
  expires_at?: Date;
}

export class ProjectApi extends AbstractApi {
  /**
   * @internal
   */
  static toProjectId(idOrNamespace: number|string, projectName: undefined|string): string {
    if (typeof idOrNamespace === 'number') {
      return `${idOrNamespace}`;
    }
    else {
      return encodeURIComponent(`${idOrNamespace}/${projectName}`);
    }
  }

  constructor(config: Config, id: number);
  constructor(config: Config, namespace: string, projectName: string);
  constructor(config: Config, idOrNamespace: number|string, projectName?: string) {
    super(config, `/projects/${ProjectApi.toProjectId(idOrNamespace, projectName)}`);
  }

  get(): Promise<Project> {
    return this.fetch('GET', undefined);
  }

  edit(params: EditProjectParams = {}): Promise<Project> {
    return this.fetch('PUT', undefined, params);
  }

  delete(): Promise<Project> {
    return this.fetch('DELETE', undefined);
  }

  star(): Promise<Project> {
    return this.fetch('POST', '/star');
  }

  unstar(): Promise<Project> {
    return this.fetch('DELETE', '/star');
  }

  archive(): Promise<Project> {
    return this.fetch('POST', '/archive');
  }

  unarchive(): Promise<Project> {
    return this.fetch('POST', '/unarchive');
  }

  getUsers(): Promise<UserSummary[]> {
    return this.fetch('GET', '/users');
  }

  getEvents(): Promise<Event[]> {
    return this.fetch('GET', '/events');
  }

  uploadFile(file: string): Promise<UploadedFile> {
    return this.fetch('POST', '/uploads', { file });
  }

  share(groupId: number, groupAccess: number, params: ShareProjectParams = {}): Promise<void> {
    return this.fetch('POST', '/share', Object.assign({ group_id: groupId, group_access: groupAccess }, params));
  }

  unshare(groupId: number): Promise<void> {
    return this.fetch('DELETE', `/share/${groupId}`);
  }

  createForkRelation(forkedFromId: number): Promise<void> {
    return this.fetch('POST', `/fork/${forkedFromId}`);
  }
  deleteForkRelation(): Promise<void> {
    return this.fetch('DELETE', `/fork`);
  }
}
