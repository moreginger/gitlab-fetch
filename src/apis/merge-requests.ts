import { AbstractApi, Config } from '../common';
import { MergeRequest, Commit, Issue, Todo, MergeRequestVersionSummary, MergeRequestVersion } from '../models';
import { ProjectApi } from './project';

export interface ListMergeRequestsParams {
  iid?: number[];
  state?: 'all' | 'merged' | 'opened' | 'closed';
  order_by?: 'created_at' | 'updated_at';
  sort?: 'asc' | 'desc';
}

export interface CreateMergeRequestsParams {
  assignee_id?: number;
  description?: string;
  target_project_id?: string;
  labels?: string[];
  milestone_id?: number;
  remove_source_branch?: true | undefined;
}

export interface EditMergeRequestParams extends CreateMergeRequestsParams {
  source_branch?: string;
  target_branch?: string;
  title?: string;
}

export interface AcceptMergeRequestParams {
  merge_commit_message?: string;
  should_remove_source_branch?: boolean;
  merge_when_build_succeeds?: boolean;
  sha?: string;
}

export class MergeRequestsApi extends AbstractApi {
  constructor(config: Config, id: number);
  constructor(config: Config, namespace: string, projectName: string);
  constructor(config: Config, idOrNamespace: number|string, projectName?: string) {
    super(config, `/projects/${ProjectApi.toProjectId(idOrNamespace, projectName)}/merge_requests`);
  }

  getAll(params: ListMergeRequestsParams = {}): Promise<MergeRequest[]> {
    return this.fetch('GET', undefined, params);
  }
  get(id: number): Promise<MergeRequest> {
    return this.fetch('GET', `/${id}`);
  }
  getCommits(id: number): Promise<Commit[]> {
    return this.fetch('GET', `/${id}/commits`);
  }
  getChanges(id: number): Promise<MergeRequest> {
    return this.fetch('GET', `/${id}/changes`);
  }
  getIssues(id: number): Promise<Issue[]> {
    return this.fetch('GET', `/${id}/closes_issues`);
  }
  getVersions(id: number): Promise<MergeRequestVersionSummary> {
    return this.fetch('GET', `/${id}/versions`);
  }
  getVersion(id: number, versionId: number): Promise<MergeRequestVersion> {
    return this.fetch('GET', `/${id}/versions/${versionId}`);
  }
  create(sourceBranch: string, targetBranch: string, title: string, params: CreateMergeRequestsParams = {}): Promise<MergeRequest> {
    return this.fetch('POST', undefined, Object.assign({ source_branch: sourceBranch, target_branch: targetBranch, title }, params));
  }
  edit(id: number, params: EditMergeRequestParams = {}): Promise<MergeRequest> {
    return this.fetch('PUT', `/${id}`, params);
  }
  delete(id: number): Promise<void> {
    return this.fetch('DELETE', `/${id}`);
  }
  accept(id: number, params: AcceptMergeRequestParams = {}): Promise<MergeRequest> {
    return this.fetch('PUT', `/${id}/merge`, params);
  }
  cancel(id: number): Promise<MergeRequest> {
    return this.fetch('PUT', `/${id}/cancel_merge_when_build_succeeds`);
  }
  subscribe(id: number): Promise<MergeRequest> {
    return this.fetch('POST', `/${id}/subscription`);
  }
  unsubscribe(id: number): Promise<MergeRequest> {
    return this.fetch('DELETE', `/${id}/subscription`);
  }
  createTodo(id: number): Promise<Todo<MergeRequest>> {
    return this.fetch('POST', `/${id}/todo`);
  }
}
