import { AbstractApi, Config } from '../common';

import { User, UserSummary } from '../models';

export interface GetUsersParams {
  active?: boolean;
  blocked?: boolean;
  search?: string;
  username?: string;
  external?: boolean;
}

export interface GetCurrentUserParams {
  sudo?: number;
}

export class UsersApi extends AbstractApi {
  constructor(config: Config) {
    super(config, '/users');
  }

  getAll(params: GetUsersParams = {}): Promise<(UserSummary[])|(User[])> {
    return this.fetch('GET', undefined, params);
  }

  get(id: number): Promise<UserSummary|User> {
    return this.fetch('GET', `/${id}`);
  }

  getCurrent(params: GetCurrentUserParams = {}): Promise<User> {
    return this.fetch('GET', undefined, params, { baseApiPath: '/user' });
  }

  // create(): Promise<User> {
  //   TODO
  // }

  // edit(id: number): Promise<User> {
  //   TODO
  // }

  delete(id: number): Promise<void> {
    return this.fetch('DELETE', `/${id}`);
  }

  getSSHKeys(): Promise<Object[]> {
    return this.fetch('GET', '/keys', undefined, { baseApiPath: '/user' });
  }
}
